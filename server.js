const express = require('express');
const app = express();         
const bodyParser = require('body-parser');
const port = 3000;
const mssql = require('mssql');
const cors = require('cors');

const db = {
    server: '10.10.0.4',
    port: 1320,
    database: 'TESTE',  
    user: 'sa',
    pass: '102010'
};

// String Connection
const connStr = `Server=${db.server},${db.port};Database=${db.database};User Id=${db.user};Password=${db.pass}`;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Cors Setup - *All rules
app.use(cors());

const router = express.Router();
router.get('/', (req, res) => res.json({ message: `Servidor rodando na porta ${port}`}));

router.post('/recursos', (req, res) => {

    const table = 'SH1010';
    const filial = req.body.filial;

    let sql = `SELECT H1_FILIAL FILIAL, H1_CODIGO CODIGO, RTRIM(H1_DESCRI) DESCRICAO,`;
    sql += ` H1_CCUSTO CCUSTO FROM ${table} WHERE D_E_L_E_T_ <> '*' AND H1_FILIAL = '${filial}'`;
    sql += ` AND H1_CCUSTO IN ('3201', '3301', '3401')`;
    
    execSQLQuery(sql, res);
});

router.get('/produtos', (req, res) => {

    const table = 'SB1010';
    //const filial = req.body.filial;

    let sql = `SELECT RTRIM(B1_COD) CODIGO, RTRIM(B1_DESC) DESCRICAO, RTRIM(B1_CC) CCUSTO`;
    sql += ` FROM ${table} WHERE D_E_L_E_T_ <> '*' AND B1_MSBLQL = '2' AND B1_TIPO IN('PA','PI') AND B1_CC IN ('3201','3301','3401')`;
    
    execSQLQuery(sql, res);
});

router.get('/custos', (req, res) => {

    const table = 'CTT010';

    let sql = `SELECT RTRIM(CTT_CUSTO) CCUSTO, RTRIM(CTT_DESC01) DESCRICAO`;
    sql += ` FROM CTT010 WHERE D_E_L_E_T_ <> '*' AND CTT_CUSTO IN ('3201', '3301', '3401')`;
    
    execSQLQuery(sql, res);
});

router.post('/motivos', (req, res) => {

    const table1 = 'ZAQ010';
    const table2 = 'SX5010';
    const filial = req.body.filial;

    let sql = `SELECT RTRIM(X5_FILIAL) FILIAL, RTRIM(ZAQ_CCUSTO) CCUSTO,`;
    sql += ` RTRIM(X5_CHAVE) MOTIVO, RTRIM(X5_DESCRI) DESCRICAO`;
    sql += ` FROM ${table1} ZAQ`;
    sql += ` INNER JOIN ${table2} X5 ON`;
    sql += ` X5_FILIAL = ZAQ_FILIAL AND ZAQ_MOTIVO = X5_CHAVE AND X5_TABELA = '44' AND X5.D_E_L_E_T_ <> '*' WHERE ZAQ_FILIAL = '${filial}' AND ZAQ_CCUSTO IN ('3201','3301','3401')`;
    
    execSQLQuery(sql, res);
});

router.post('/funcionarios', (req, res) =>{

    const table = 'SRA010';
    const filial = req.body.filial;
    const matricula = req.body.matricula;
    let sql;

    if (matricula) {
        sql = `SELECT RA_FILIAL FILIAL, RA_MAT MATRICULA, RTRIM(RA_NOME) NOME FROM ${table}`;
        sql += ` WHERE RA_FILIAL = '${filial}' AND D_E_L_E_T_ <> '*' AND RA_SITFOLH NOT IN ('D', 'T') AND RA_MAT = '${matricula}'`; 
    } else {
        sql = `SELECT RA_FILIAL FILIAL, RA_MAT MATRICULA, RTRIM(RA_NOME) NOME FROM ${table}`;
        sql += ` WHERE RA_FILIAL = '${filial}' AND D_E_L_E_T_ <> '*' AND RA_SITFOLH NOT IN ('D', 'T')`
    }

    execSQLQuery(sql, res);
});

router.post('/eficiencia', (req, res) => {

    const table = 'TBAPPE';

    const dt_ini = req.body.dt_ini;
    const hr_ini = req.body.hr_ini;
    const filial = req.body.filial;
    const recurso = req.body.recurso;
    const produto = req.body.produto;
    const turno = req.body.turno;
    const turma = req.body.turma;
    const quantidade = parseFloat(req.body.quantidade);
    const resultado = req.body.resultado;
    const motivo = req.body.motivo;
    const custo = req.body.custo;
    const operador = req.body.operador;
    const usuario = req.body.usuario;


    //console.log(JSON.stringify(req.body));

    let sql = `INSERT INTO ${table} (dt_ini, hr_ini, filial, recurso, produto, turno, turma, quantidade, resultado, motivo, ccusto, operador, usuario)`;
    sql += ` VALUES ('${dt_ini}', '${hr_ini}', '${filial}', '${recurso}', '${produto}', '${turno}', '${turma}', '${quantidade}', '${resultado}', '${motivo}', '${custo}', '${operador}', '${usuario}')`

    console.log(sql);

    execSQLQuery(sql, res);
});

router.post('/improdutivo', (req, res) => {

    console.log(JSON.stringify(req.body));

    const table = 'TBAPPI';

    const filial = req.body.cabecalho.filial;
    const dt_ini = req.body.cabecalho.dt_ini;
    const dt_final = req.body.cabecalho.dt_final;
    const hr_ini = req.body.cabecalho.hr_ini;
    const hr_final = req.body.cabecalho.hr_final;
    const ccusto = req.body.cabecalho.ccusto;
    const motivo = req.body.cabecalho.motivo;
    const recurso = req.body.corpo.codigo;
    const usuario = req.body.cabecalho.usuario;

    let sql = `INSERT INTO ${table} (filial, dt_ini, dt_final, hr_ini, hr_final, ccusto, motivo, recurso, usuario)`;
    sql += ` VALUES('${filial}', '${dt_ini}', '${dt_final}', '${hr_ini}', '${hr_final}', '${ccusto}', '${motivo}', '${recurso}', '${usuario}')`;

    console.log(sql);
    
    execSQLQuery(sql, res);
});

app.use('/', router);

// Conexão global
mssql.connect(connStr)
   .then(conn => {
        global.conn = conn;

        // Inicia o servidor
        app.listen(port, () => {
            console.log(`Servidor rodando na porta ${port}`);
            console.log('Pressione CTRL + C para encerrar a execução');
        });
   })
   .catch(err => console.log(err));

function execSQLQuery(sql, res){
    global.conn.request()
               .query(sql)
               .then(result => res.json(result.recordset))
               .catch(err => res.json(err));
}
